FROM amazoncorretto:21-alpine AS BUILD

ENV LIBJPEGTURBO_VERSION=2.1.5.1

RUN apk --no-cache add \
  nasm \
  make \
  cmake \
  build-base

# Install libjpeg-turbo WITH Java bindings
RUN wget https://github.com/libjpeg-turbo/libjpeg-turbo/archive/refs/tags/$LIBJPEGTURBO_VERSION.zip \
  && unzip $LIBJPEGTURBO_VERSION.zip \
  && rm $LIBJPEGTURBO_VERSION.zip \
  && cd libjpeg-turbo-$LIBJPEGTURBO_VERSION \
  && mkdir build && cd build \
  && cmake -G"Unix Makefiles" -D WITH_JAVA=1 -D WITH_TURBOJPEG=1 .. \
  && make -j8 \
  && make install \
  && ln -s /opt/libjpeg-turbo/lib64 /opt/libjpeg-turbo/lib


FROM amazoncorretto:21-alpine AS RUNTIME

ENV CANTALOUPE_VERSION=5.0.6

# Default expose
EXPOSE 8182

# Add dependencies
RUN apk --no-cache add \
  ffmpeg \
  graphicsmagick \
  openjpeg-tools

# Add cantaloupe user
RUN adduser --system cantaloupe

# Get Cantaloupe
RUN wget https://github.com/cantaloupe-project/cantaloupe/releases/download/v$CANTALOUPE_VERSION/cantaloupe-$CANTALOUPE_VERSION.zip \
  && unzip cantaloupe-$CANTALOUPE_VERSION.zip \
  && ln -s cantaloupe-$CANTALOUPE_VERSION cantaloupe \
  && rm cantaloupe-$CANTALOUPE_VERSION.zip \
  && mkdir -p /var/log/cantaloupe /var/cache/cantaloupe \
  && chown -R cantaloupe /cantaloupe /var/log/cantaloupe /var/cache/cantaloupe \
  && cp -rs /cantaloupe/deps/Linux-x86-64/* /usr/ \
  && cp /cantaloupe/cantaloupe.properties.sample /cantaloupe/cantaloupe.properties

# Copy from build stage
COPY --from=BUILD /opt/libjpeg-turbo/lib64/libturbojpeg.so /opt/libjpeg-turbo/lib/libturbojpeg.so

# You'll need this
ENV JAVA_ARGS="-Xms4096m -Xmx4096m"

USER cantaloupe

WORKDIR /cantaloupe

ENTRYPOINT java -jar -Dcantaloupe.config=cantaloupe.properties cantaloupe-$CANTALOUPE_VERSION.jar
