# Cantaloupe IIIF Docker

## Cantaloupe 🍈

This image is built on top of the OpenJDK Alpine image, which makes it very lightweight. It includes libjpeg-turbo to speed up JPEG rendering.

## Image

You can use the pre-built docker image in the container registry of this gitlab repository. A docker-compose example:

```yml
version: '3.9'
services:
  iiif:
    image: registry.gitlab.com/uvacreate/vondel/cantaloupe-docker:v5.0.6
    ports:
      - 8182:8182
    volumes:
      - ./images:/images/
    environment:
      # - HTTP_PORT=9000
      # - "SLASH_SUBSTITUTE=:"
      - FILESYSTEMSOURCE_BASICLOOKUPSTRATEGY_PATH_PREFIX=/images/
      # And many more options that you would normally add to the cantaloupe.properties file. 
      # See: https://cantaloupe-project.github.io/
```

